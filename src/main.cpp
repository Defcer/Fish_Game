#include "glew.h"
#include "freeglut.h"
#include "glm.hpp"
#include "ext.hpp"
#include <iostream>
#include <cmath>
#include <string>
#include <vector>
#include <Windows.h>
#include <iomanip>

#include "Shader_Loader.h"
#include "Render_Utils.h"
#include "Camera.h"
#include "Texture.h"

GLuint programColor;
GLuint programTexture;

Core::Shader_Loader shaderLoader;

obj::Model fishModel;
std::vector<obj::Model> vecFish;
obj::Model sphereModel;
obj::Model groundModel;
obj::Model grassModel;
obj::Model rockModel;
obj::Model flowerModel;

const int VALUE_OF_FISH = 80;
float angle[VALUE_OF_FISH];
glm::vec3 fishPosition[VALUE_OF_FISH];
glm::vec3 fishColor[VALUE_OF_FISH];

glm::vec3 cameraPos = glm::vec3(0, 0, 5);
glm::vec3 cameraDir; // Wektor "do przodu" kamery
glm::vec3 cameraSide; // Wektor "w bok" kamery
float cameraAngle = 0;

glm::mat4 cameraMatrix, perspectiveMatrix;

glm::vec3 lightDir = glm::normalize(glm::vec3(1.0f, -0.9f, -1.0f));

glm::quat rotation = glm::quat(1, 0, 0, 0);

GLuint textureAsteroid;
GLuint textureGrass;
GLuint textureGround;
GLuint textureRock;

int xChange;
int yChange;
int i = 0;

void keyboard(unsigned char key, int x, int y)
{
	float angleSpeed = 0.1f;
	float moveSpeed = 0.1f;
	switch (key)
	{
	case 'z': cameraAngle -= angleSpeed; break;
	case 'x': cameraAngle += angleSpeed; break;
	case 'w': cameraPos += cameraDir * moveSpeed; break;
	case 's': cameraPos -= cameraDir * moveSpeed; break;
	case 'd': cameraPos += cameraSide * moveSpeed; break;
	case 'a': cameraPos -= cameraSide * moveSpeed; break;
	}
}

void mouse(int x, int y)
{
	static int xOld = x;
	static int yOld = y;
	xChange = x - xOld;
	yChange = y - yOld;
	xOld = x;
	yOld = y;
}

glm::mat4 createCameraMatrix()
{
	cameraDir = glm::vec3(cosf(cameraAngle - glm::radians(90.0f)), 0.0f, sinf(cameraAngle - glm::radians(90.0f)));
	glm::vec3 up = glm::vec3(0, 1, 0);
	cameraSide = glm::cross(cameraDir, up);

	return Core::createViewMatrix(cameraPos, cameraDir, up);
}

void drawObjectColor(obj::Model * model, glm::mat4 modelMatrix, glm::vec3 color)
{
	GLuint program = programColor;

	glUseProgram(program);

	glUniform3f(glGetUniformLocation(program, "objectColor"), color.x, color.y, color.z);
	glUniform3f(glGetUniformLocation(program, "lightDir"), lightDir.x, lightDir.y, lightDir.z);

	glm::mat4 transformation = perspectiveMatrix * cameraMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(program, "modelViewProjectionMatrix"), 1, GL_FALSE, (float*)&transformation);
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, GL_FALSE, (float*)&modelMatrix);

	Core::DrawModel(model);

	glUseProgram(0);
}

void drawObjectTexture(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId)
{
	GLuint program = programTexture;

	glUseProgram(program);

	glUniform3f(glGetUniformLocation(program, "lightDir"), lightDir.x, lightDir.y, lightDir.z);
	Core::SetActiveTexture(textureId, "textureSampler", program, 0);

	glm::mat4 transformation = perspectiveMatrix * cameraMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(program, "modelViewProjectionMatrix"), 1, GL_FALSE, (float*)&transformation);
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, GL_FALSE, (float*)&modelMatrix);

	Core::DrawModel(model);

	glUseProgram(0);
}
void renderScene()
{
	// Aktualizacja macierzy widoku i rzutowania
	cameraMatrix = createCameraMatrix();
	perspectiveMatrix = Core::createPerspectiveMatrix();

	glClearColor(0.0f, 0.7f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 shipModelMatrix = glm::translate(cameraPos + cameraDir * 0.5f + glm::vec3(0, -1, -2)) * glm::rotate(-cameraAngle + 2 * glm::radians(90.0f), glm::vec3(0, 1, 0))* glm::scale(glm::vec3(0.5f));

	if (i == 121)
	{
		i = 0;
	}
	if (i == i)
	{
		//glowna rybka
		drawObjectColor(&vecFish[i], shipModelMatrix, glm::vec3(0.6f));

		//pozostale rybki
		for (int j = 0; j < VALUE_OF_FISH; j++)
		{
			glm::mat4 modelMatrix = glm::rotate(glm::radians(angle[j]), glm::vec3(0, 1, 0));
			drawObjectColor(&vecFish[i], glm::translate(fishPosition[j]) * 1 * modelMatrix, glm::vec3(0.4f));
		}

		Sleep(4);
	}
	i++;

	drawObjectTexture(&groundModel, glm::translate(glm::vec3(0, -2, 0)), textureGround);

	glutSwapBuffers();
}

void readFish()
{
	std::string a = "models/fish/untitled_000";
	std::string b = ".obj";
	std::string path = "";

	for (int j = 0; j < 121; j++)
	{
		std::stringstream ss;
		ss << j;
		std::string str = ss.str();
		if (j<10) { path = a + "00" + str + b; }
		else
			if (j<100) { path = a + "0" + str + b; }
			else
			{
				path = a + str + b;
			}
		std::cout << path << std::endl;

		fishModel = obj::loadModelFromFile(path);
		vecFish.push_back(fishModel);
	}
}

void init()
{
	srand(time(NULL));
	glEnable(GL_DEPTH_TEST);
	programColor = shaderLoader.CreateProgram("shaders/shader_color.vert", "shaders/shader_color.frag");
	programTexture = shaderLoader.CreateProgram("shaders/shader_tex.vert", "shaders/shader_tex.frag");

	readFish();
	sphereModel = obj::loadModelFromFile("models/sphere.obj");
	groundModel = obj::loadModelFromFile("models/untitled.obj");

	textureAsteroid = Core::LoadTexture("textures/asteroid.png");
	textureGround = Core::LoadTexture("textures/sand.png");

	for (int j = 3; j < VALUE_OF_FISH; j++)
	{
		int colX = (rand() % 9);
		int colY = (rand() % 9);
		int colZ = (rand() % 9);

		float fColX = (float)colX / 10;
		float fColY = (float)colX / 10;
		float fColZ = (float)colX / 10;

		fishColor[j - 1] = glm::vec3(fColX, fColY, fColZ);
		fishColor[j - 3] = glm::vec3(fColX, fColY, fColZ);
		fishColor[j - 2] = glm::vec3(fColX, fColY, fColZ);
		fishColor[j] = glm::vec3(fColX, fColY, fColZ);

		angle[j - 3] = (float)(rand() % 360) + 1;
		angle[j - 2] = (float)(rand() % 360) + 1;
		angle[j - 1] = (float)(rand() % 360) + 1;
		angle[j] = (float)(rand() % 360) + 1;

		fishPosition[j - 1] = glm::vec3((rand() % 46) - 50, (rand() % 10), (rand() % 46) - 50);
		fishPosition[j - 2] = glm::vec3((rand() % 46) - 3, (rand() % 10), (rand() % 46) - 1);
		fishPosition[j - 3] = glm::vec3((rand() % 46) - 50, (rand() % 10), (rand() % 46) - 1);
		fishPosition[j] = glm::vec3((rand() % 46) - 3, (rand() % 10), (rand() % 46) - 50);
		j += 4;
	}
}

void shutdown()
{
	shaderLoader.DeleteProgram(programColor);
	shaderLoader.DeleteProgram(programTexture);
}

void idle()
{
	glutPostRedisplay();
}

int main(int argc, char ** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(200, 100);
	glutInitWindowSize(600, 600);
	glutCreateWindow("OpenGL Podwodny Program");
	glewInit();
	init();
	glutKeyboardFunc(keyboard);
	glutPassiveMotionFunc(mouse);
	glutDisplayFunc(renderScene);
	glutIdleFunc(idle);

	glutMainLoop();

	shutdown();

	return 0;
}
